const { app, BrowserWindow } = require("electron");
const isDev = require("electron-is-dev");
const path = require("path");
const { ipcMain } = require("electron");
const { shell } = require("electron");

let mainWindow;

function createWindow() {
  mainWindow = new BrowserWindow({
    width: 400,
    height: 260,
    show: false,
    minHeight: 224,
    minWidth: 284,
  });
  const startURL = isDev
    ? "http://localhost:3000"
    : `file://${path.join(__dirname, "../build/index.html")}`;

  mainWindow.loadURL(startURL);

  mainWindow.once("ready-to-show", () => mainWindow.show());
  mainWindow.on("closed", () => {
    mainWindow = null;
  });

  //“ipcMain” should be use here
  ipcMain.on("resize", function (e, x, y) {
    mainWindow.setSize(x, y);
  });
}
app.on("ready", createWindow);
ipcMain.on("loadGH", (event, arg) => {
  shell.openExternal(arg);
});
