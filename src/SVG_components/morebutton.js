import React, { PureComponent } from "react";
// Icon
class MoreButton extends PureComponent {
  render() {
    return (
      <svg
        className={this.props.className}
        version="1.1"
        id="Capa_1"
        xmlns="http://www.w3.org/2000/svg"
        xlink="http://www.w3.org/1999/xlink"
        x="0px"
        y="0px"
        viewBox="0 0 512 512"
        fill={this.props.fill}
        space="preserve"
      >
        <g>
          <g>
            <g>
              <circle cx="256" cy="256" r="64" />
              <circle cx="256" cy="448" r="64" />
              <circle cx="256" cy="64" r="64" />
            </g>
          </g>
        </g>
      </svg>
    );
  }
}

export default MoreButton;
