import React from "react";
import { Route, Redirect } from "react-router-dom";
import { useAuth } from "./MyContext";
function PrivateRoute({ component: Fry, ...rest }) {
  const isAuthenticated = useAuth();
  return (
    <Route
      {...rest}
      render={(props) =>
        isAuthenticated.loggedIn ? (
          <Fry {...props} token={isAuthenticated.token} />
        ) : (
          <Redirect to="/" />
        )
      }
    />
  );
}

export default PrivateRoute;
