import React from "react";

class Login extends React.Component {
  render() {
    return (
      <div className="signPage">
        <h1 className="signin-title">Mini-Spotify App</h1>
        <button className="signButton">
          <a href={this.props.url}>Sign In</a>
        </button>
      </div>
    );
  }
}

export default Login;
