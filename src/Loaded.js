import React from "react";
import axios from "axios";
import Logo from "./Imgs/Spotify_Logo_CMYK_White.png";
import Heart from "./SVG_components/heart";
import DropDown from "./Dropdown";
import Transport from "./Transport";

class Loaded extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isError: false,
      loading: false,
      loaded: false,
      data: null,
      clicked: false,
      liked: false,
      id: null,
      fill: "#333333",
      playlists: [],
      selectedPlaylist: null,
    };
  }

  componentDidMount() {
    const url = "https://api.spotify.com/v1/me/playlists";
    let config = {
      method: "GET",
      headers: {
        authorization: "Bearer " + this.props.token,
        "content-type": "application/x-www-form-urlencoded",
      },
      url,
    };
    this.callApi(config);
  }

  componentDidUpdate() {
    if (this.props.data.currently_playing_type !== "ad") {
      const url =
        "https://api.spotify.com/v1/me/tracks/contains?ids=" +
        this.props.data.item.id;
      let config = {
        method: "GET",
        headers: {
          authorization: "Bearer " + this.props.token,
          "content-type": "application/x-www-form-urlencoded",
        },
        url,
      };
      console.log("Loaded config for contains", config);
      if (this.props.data.item.id !== this.state.id) {
        console.log("API called");
        this.callApi(config);
        this.setState({ id: this.props.data.item.id });
      }
    } // if is not an ad
  }

  callApi(config) {
    console.log(config);
    axios(config)
      .then((result) => {
        if (result.status === 200) {
          this.setState({ loaded: true, loading: false });
          if (result.data[0] === true) {
            this.setState({ liked: true });
            this.setState({ data: result.data });
            this.updatefill();
          }
          if (result.config.url === "https://api.spotify.com/v1/me/playlists") {
            this.setState({ playlists: result.data.items });
          }
        }
      })
      .catch((e) => {
        this.setState({ isError: true });
      })
      .then(() => {});
  }
  updatefill() {
    if (this.state.data[0] === true) {
      console.log("Adata is true");
      this.setState({ fill: "#DC143C" });
    }
  }

  myCallback = (playlistFromChild) => {
    this.setState({ selectedPlaylist: playlistFromChild });
  };

  addSongToPlaylist() {
    const url =
      "https://api.spotify.com/v1/playlists/" +
      this.state.selectedPlaylist +
      "/tracks";
    let config = {
      method: "POST",
      headers: {
        authorization: "Bearer " + this.props.token,
        "content-type": "application/x-www-form-urlencoded",
      },
      data: {
        uris: [this.props.data.item.uri],
      },
      url,
    };
    this.callApi(config);
  }

  handlePlay() {
    const url = "https://api.spotify.com/v1/me/player/play";
    let config = {
      method: "PUT",
      headers: {
        authorization: "Bearer " + this.props.token,
        "content-type": "application/x-www-form-urlencoded",
      },
      url,
    };
    this.callApi(config);
  }

  handleSkip() {
    const url = "https://api.spotify.com/v1/me/player/next";
    let config = {
      method: "POST",
      headers: {
        authorization: "Bearer " + this.props.token,
        "content-type": "application/x-www-form-urlencoded",
      },
      url,
    };
    this.callApi(config);
  }

  handlePrevious() {
    const url = "https://api.spotify.com/v1/me/player/previous";
    let config = {
      method: "POST",
      headers: {
        authorization: "Bearer " + this.props.token,
        "content-type": "application/x-www-form-urlencoded",
      },
      url,
    };
    this.callApi(config);
  }

  handleLikes(token, id) {
    this.setState({ clicked: !this.state.clicked });
    if (!this.state.liked) {
      const url = "https://api.spotify.com/v1/me/tracks";
      let config = {
        method: "PUT",
        headers: {
          authorization: "Bearer " + token,
          "content-type": "application/x-www-form-urlencoded",
        },
        data: { ids: [id] },
        url,
      };
      this.callApi(config);
      this.setState({ liked: true });
      this.setState({ fill: "#DC143C" });
    } else {
      const url = "https://api.spotify.com/v1/me/tracks";
      let config = {
        method: "DELETE",
        headers: {
          authorization: "Bearer " + token,
          "content-type": "application/x-www-form-urlencoded",
        },
        data: { ids: [id] },
        url,
      };
      this.callApi(config);
      this.setState({ liked: false });
      this.setState({ fill: "#333333" });
    }
  }

  setTempfill() {
    if (this.state.liked !== true) {
      this.setState({ fill: "#ffa9a3" });
    }
  }

  resetTempFill() {
    if (this.state.liked !== true) {
      this.setState({ fill: "#333333" });
    } else {
      this.setState({ fill: "#DC143C" });
    }
  }

  handleBoxEnter = () => this.setTempfill();
  handleBoxLeave = () => this.resetTempFill();

  render() {
    if (this.props.data.currently_playing_type === "ad") {
      return (
        <div>
          <h1 className="ad_text">AD IS PLAYING</h1>
        </div>
      );
    } else {
      return (
        <div className="Loaded">
          <div className="upperBar">
            <img src={Logo} alt="logo" className="logo"></img>
          </div>
          <div className="container">
            <div className="child1">
              <img
                src={this.props.data.item.album.images[0].url}
                alt="album cover"
                className="albumImg"
              ></img>
              <DropDown
                title="Select Movie"
                items={this.state.playlists}
                addSongToPlaylist={this.addSongToPlaylist.bind(this)}
                callbackFromParent={this.myCallback}
              />
            </div>

            <div className="child2">
              <div>
                <h1 className="trackName">{this.props.data.item.name}</h1>
                <h3 className="artistName">
                  {this.props.data.item.artists[0].name}
                </h3>
                <div classname="heartHolder">
                  <Heart
                    className="heart"
                    fill={this.state.fill}
                    onClick={() =>
                      this.handleLikes(
                        this.props.token,
                        this.props.data.item.id
                      )
                    }
                    onMouseEnter={this.handleBoxEnter}
                    onMouseLeave={this.handleBoxLeave}
                  />
                </div>
              </div>
              <Transport
                play={false}
                playMethod={this.handlePlay.bind(this)}
                skipMethod={this.handleSkip.bind(this)}
                previousmethod={this.handlePrevious.bind(this)}
              />
            </div>
          </div>
        </div>
      );
    }
  }
}
export default Loaded;
