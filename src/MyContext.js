import { createContext, useContext } from "react";

export const myContext = createContext();

const AuthContextConsumer = myContext.Consumer;

export { AuthContextConsumer };

export function useAuth() {
  return useContext(myContext);
}
