import React from "react";
import axios from "axios";
import Loaded from "./Loaded";
class Protected extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isError: false,
      loading: false,
      loaded: false,
      data: null,
      flag: false,
      timeLeft: 5000,
      clicked: false,
    };

    if (this.state.loaded)
      setInterval(this.getSong(this.props.token), this.state.timeLeft);
  }

  getSong(token) {
    const url = "https://api.spotify.com/v1/me/player/currently-playing";
    const config = {
      headers: { Authorization: "Bearer " + token },
      mode: "cors",
      cache: "default",
    };

    axios
      .get(url, config)
      .then((result) => {
        console.log(" P1 data in Protected : ", result.data);
        if (result.status === 200) {
          this.setState({ loading: false, data: result.data });
          this.setState({ loaded: true });
          this.setState({
            timeLeft: result.data.item.duration_ms - result.data.progress_ms,
          });
        }
      })
      .catch((e) => {
        this.setState({ isError: true });
      })
      .then(() => {
        this.setState({ redirect: true });
      });

    this.callNextSong();
  }

  componentWillMount() {
    this.getSong(this.props.token);
  }

  callNextSong() {
    this.timer = setTimeout(
      () => this.getSong(this.props.token),
      this.state.timeLeft
    );
  }

  render() {
    if (this.state.loaded === false) {
      return <Loading />;
    }
    if (this.state.loaded === true) {
      return (
        <Loaded
          data={this.state.data}
          clicked={this.state.clicked}
          token={this.props.token}
        />
      );
    }
  }
}

function Loading(props) {
  return <h1>Welcome back!</h1>;
}

export default Protected;
