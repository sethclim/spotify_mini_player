import React from "react";
import { myContext } from "./MyContext";
export class AuthProvider extends React.Component {
  constructor() {
    super();
    this.state = {
      token: "",
      loggedIn: false,
    };
    this.setLoggedIn = this.setLoggedIn.bind(this);
    this.setToken = this.setToken.bind(this);
  }

  setToken(e) {
    this.setState({
      token: e,
    });
  }
  setLoggedIn(e) {
    this.setState({
      loggedIn: e,
    });
  }

  render() {
    return (
      <myContext.Provider
        value={{
          ...this.state,
          setToken: this.setToken,
          setLoggedIn: this.setLoggedIn,
        }}
      >
        {this.props.children}
      </myContext.Provider>
    );
  }
}
