import React from "react";
import axios from "axios";
import { myContext } from "./MyContext";
import { Redirect } from "react-router-dom";
const clientID = "d7c02eeea24f4bb1953ede37337c9e68";
const headers = {
  "Content-Type": "application/json;charset=UTF-8",
  "Access-Control-Allow-Origin": "*",
  "Access-Control-Allow-Headers": "Content-Type",
  "Access-Control-Allow-Methods": "GET,POST,PUT,DELETE,OPTIONS",
  "Access-Control-Allow-Credentials": true,
};

export default class Api extends React.Component {
  static contextType = myContext;
  constructor(props) {
    super(props);
    this.state = {
      isLoggedIn: false,
      isError: false,
      loading: false,
      data: null,
      verifier: props.verifier,
      redirect: false,
      redirectpath: "/protected",
    };
  }

  componentDidMount() {
    this.setState({ loading: true });
    const token = this.props.token.code;
    console.log("verifier in API", this.state.verifier);
    const url = "https://accounts.spotify.com/api/token";
    const content =
      "&client_id=" +
      clientID +
      "&grant_type=authorization_code" +
      "&code=" +
      token +
      "&redirect_uri=http://localhost:3000/redirect" +
      "&code_verifier=" +
      this.state.verifier;

    console.log(url, content, headers);
    axios
      .post(url, content)
      .then((result) => {
        console.log("data : ", result.data);
        if (result.status === 200) {
          this.setState({ isLoggedIn: true });
          this.setState({ loading: false, data: result.data });
          this.context.setToken(this.state.data.access_token);
          this.context.setLoggedIn(this.state.isLoggedIn);
        } else {
          this.setState({ isError: true });
        }
      })
      .catch((e) => {
        this.setState({ isError: true });
      })
      .then(() => {
        this.setState({ redirect: true });
      });
  }
  render() {
    if (this.state.redirect) {
      return <Redirect to="/protected" />;
    } else {
      return (
        <div>
          {this.state.isError && (
            <div>The username or password provided were incorrect!</div>
          )}
        </div>
      );
    }
  }
}
