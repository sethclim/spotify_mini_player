// src/App.js
import React from "react";
import Login from "./Login";
import Page from "./page";
import Protected from "./Protected";
import "./App.css";
import PrivateRoute from "./PrivateRoute";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { AuthProvider } from "./ContextProvider";
var CryptoJS = require("crypto-js");
function generateVer() {
  function generateRandomString(length) {
    var text = "";
    var possible =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~";
    for (var i = 0; i < length; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }
  function base64URLEncode(str) {
    return str
      .toString(CryptoJS.enc.Base64)
      .replace(/\+/g, "-")
      .replace(/\//g, "_")
      .replace(/=/g, "");
  }
  const verifier = generateRandomString(50);
  console.log("verifier", verifier);
  function sha256(buffer) {
    return CryptoJS.SHA256(buffer);
  }
  const challenge = base64URLEncode(sha256(verifier)).toString();
  const clientId = "d7c02eeea24f4bb1953ede37337c9e68";
  const redirectURI = "http://localhost:3000/redirect";

  const authUrl =
    "https://accounts.spotify.com/authorize?" +
    "&response_type=code" +
    "&client_id=" +
    clientId +
    "&code_challenge=" +
    challenge +
    "&code_challenge_method=S256" +
    "&scope=user-read-playback-state user-modify-playback-state user-library-modify user-library-read playlist-read-private playlist-modify-public playlist-modify-private" +
    "&redirect_uri=" +
    encodeURIComponent(redirectURI);
  localStorage.setItem("url", authUrl);
  localStorage.setItem("ver", verifier);
}

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showProtected: false,
      setShowProtected: false,
      error: false,
      token: false,
    };
  }

  componentDidMount() {
    if (localStorage.getItem("url") === null) {
      generateVer();
    }
  }

  callbackFunction = (childData) => {
    this.setState({ token: true });
  };

  render() {
    return (
      <div className="App">
        <AuthProvider>
          <Router>
            <Route
              exact
              path="/"
              render={(props) => (
                <Login {...props} url={localStorage.getItem("url")} />
              )}
            />
            <Route
              path="/redirect"
              render={(props) => (
                <Page {...props} verifier={localStorage.getItem("ver")} />
              )}
            />
            <PrivateRoute path="/protected" component={Protected} />
          </Router>
        </AuthProvider>
      </div>
    );
  }
}
export default App;
