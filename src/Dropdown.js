import React, { useState } from "react";
import MoreButton from "./SVG_components/morebutton";
import CheckMark from "./SVG_components/checkMark";
import onClickOutside from "react-onclickoutside";
function DropDown({
  addSongToPlaylist,
  callbackFromParent,
  items = [],
  multiSelect,
}) {
  const [open, setOpen] = useState(false);
  const [selection, setSelection] = useState([]);
  const toggle = () => setOpen(!open);
  DropDown.handleClickOutside = () => setOpen(false);

  function handleOnClick(item) {
    if (!selection.some((current) => current.id === item.id)) {
      if (!multiSelect) {
        setSelection([item]);
        callbackFromParent(item.id);
        addSongToPlaylist();
        toggle(!open);
      } else if (multiSelect) {
        setSelection([...selection, item]);
      }
    } else {
      let selectionAfterRemoval = selection;
      selectionAfterRemoval = selectionAfterRemoval.filter(
        (current) => current.id !== item.id
      );
      setSelection([...selectionAfterRemoval]);
    }
  }

  function isItemInSelection(item) {
    if (selection.find((current) => current.id === item.id)) {
      return true;
    } else {
      return false;
    }
  }

  return (
    <div className="dd-wrapper">
      <div
        tabIndedx={0}
        className="dd-header"
        role="button"
        onKeyPress={() => toggle(!open)}
        onClick={() => toggle(!open)}
      >
        <div className="dd_header-title">
          <p className="dd_header-title-actual">
            <MoreButton className="moreButton" fill={"white"} />
          </p>
        </div>
      </div>
      {open && (
        <div className="dd-list">
          <div className="menutitle">
            <p>Add to Playlist</p>
          </div>
          {items.map((item) => (
            <div className="dd-list-item" key={item.id}>
              <button
                className="item_button"
                type="button"
                onClick={() => handleOnClick(item)}
              >
                <span>{item.name}</span>
                <span>
                  {isItemInSelection(item) && (
                    <CheckMark className="checkMark" fill={"#1DB954"} />
                  )}
                </span>
              </button>
            </div>
          ))}
        </div>
      )}
    </div>
  );
}

const clickOutideConfig = {
  handleClickOutside: () => DropDown.handleClickOutside,
};
export default onClickOutside(DropDown, clickOutideConfig);
