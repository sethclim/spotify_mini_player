import React from "react";
import Play from "./SVG_components/Play";
import Pause from "./SVG_components/Pause";
import Skip from "./SVG_components/Skip";
import Previous from "./SVG_components/Previous";
class Transport extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      play: true,
    };
  }

  handlePlay(playToggle) {
    console.log("play Toggle", playToggle);
    this.setState({ play: playToggle });
    this.props.playMethod();
  }

  render() {
    return (
      <div className="transportContainer">
        <div className="controls">
          <Previous
            className="previousButton"
            fill={"#ffffff"}
            onClick={() => this.handlePrevious()}
          />
          <div className="playButtonContainer">
            {this.state.play ? (
              <Play
                className="playButton"
                fill={"#ffffff"}
                onClick={() => this.handlePlay(!this.state.play)}
              />
            ) : (
              <Pause
                className="playButton"
                fill={"#ffffff"}
                onClick={() => this.handlePlay(!this.state.play)}
              />
            )}
          </div>
          <Skip
            className="skipButton"
            fill={"#ffffff"}
            onClick={() => this.handleSkip()}
          />
        </div>
      </div>
    );
  }
}

export default Transport;
