import React from "react";
import Api from "./API";
const queryString = require("query-string");

export default class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      token: null,
      test: false,
    };
  }

  render() {
    return (
      <div>
        {}
        <h1>HI THERE</h1>
        <Api
          token={queryString.parse(this.props.location.search)}
          verifier={this.props.verifier}
        ></Api>
      </div>
    );
  }
}
