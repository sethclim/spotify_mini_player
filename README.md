# Spotify MiniPlayer
A mini app for displaying the currently playing song on Spotify.  
Functionality includes liking and adding songs to a playlist as well as controling playback.  
Desktop version is built in electron using web-technologies.

## Run Project
**Web Version**

In the project directory, run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.<br />

**Desktop Version**
In the project directory, run:

### `npm run dev`

Runs the desktop app in development mode.<br />

## Technologies Used

-**React.JS** -**Electron** -**Axios** -**JSX** -**CSS** 

## Demo
![Spotify_small.png](https://bitbucket.org/repo/E6jG47x/images/1668531368-Spotify_small.png)

## Usage
Launch the app while a spotify song is playing and see the currently playing song continually updated. 
Like and unlike a song with heart button. Add songs to a playlist using the dropdown menu.  Transport controls can be used to skip forward and back as well as play and pause.  

## Key Project Lessons
* Implementing PKCE Authorization Flow  
* Calling an API Endpoint
* Using the Electron Framework

## Maintainers
This project is mantained by:
* [Seth Climenhaga](https://bitbucket.org/sethclim/) 
